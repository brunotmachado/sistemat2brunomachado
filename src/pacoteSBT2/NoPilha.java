/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoteSBT2;

/**
 *
 * @author Bruno Machado
 */
public class NoPilha {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private 
        int codSala;
    
    
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/
    
    public int getCodSala() {
        return this.codSala;
    }

    public void setCodSala(int codSala) {
        this.codSala = codSala;
    }
    
    
    // Método construtor 
    
    public NoPilha(int _cs){
                
        this.setCodSala(_cs);
    }
    
    
    
}
    
