package pacoteSBT2;

import java.io.File;

import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileWriter;
import java.io.PrintWriter;

import java.io.IOException;
import java.io.FileNotFoundException;

import javax.swing.JOptionPane;

/**
 *
 * @author Jefferson
 */
public class Arquivo {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private static String caminhoLeitura = "C:\\Users\\Joel\\Desktop\\UCL 2019.1\\AED2\\T2\\SistemaT2BrunoMachado\\src\\Arquivos\\arquivo.txt";
    private static String caminhoEscrita = "C:\\Users\\Joel\\Desktop\\UCL 2019.1\\AED2\\T2\\SistemaT2BrunoMachado\\src\\Logs\\log.txt";
    
    private static String linha;
    private static String[] vetorDeInformacoes;
    
    private static File f;
    
    private static FileReader fr;
    private static BufferedReader br;
    
    private static FileWriter fw;
    private static PrintWriter saida;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    public Arquivo(){
        
        
        
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para ler o arquivo texto
    public static void lerArquivo(){
        
        try{
            
            // Associando o arquivo físico com o atributo lógico 'fr'.
            // A partir de agora a manipulação do arquivo deve ser feita
            // utilizando 'fr'.
            FileReader fr = new FileReader(caminhoLeitura);
            BufferedReader br = new BufferedReader(fr); // Associar a entrada de dados
                                                        // com 'fr'.
            
            linha = br.readLine(); // Faz a leitura da primeira linha do arquivo texto.
            
            // Verifica se chegou ao final do arquivo
            while (linha != null){
                
                // Se uma linha do arquivo, contém várias informações separadas por espaço,
                // o método split separa as informações e as coloca (cada uma) no
                // atributo vetorDeInformações (posições sequenciais).
                vetorDeInformacoes = linha.split(" ");
                linha = br.readLine(); // Avança para a próxima linha do arquivo texto.
            }
            
        }catch (FileNotFoundException fne){
            JOptionPane.showMessageDialog(null, "\"" + caminhoLeitura + "\" não existe.");
        }catch (IOException ioe){
            JOptionPane.showMessageDialog(null, "Erro na leitura do arquivo.");
        }finally{

            try {
                if (br != null) br.close(); // Fecha o buffer
                if (fr != null) fr.close(); // Fecha o arquivo
            } catch (IOException ex) {
            
            }
            
        }
        
    }
    
    // Método base para geração de logs de execução, em arquivo texto.s
    public static void gerarLog(String _texto){
        
        try{
            
            f = new File (caminhoEscrita);
            
            // Caso o arquivo não existir,
            if (!f.exists())
                f.createNewFile(); // Cria o arquivo texto.
            
            // Associando o arquivo físico com o atributo lógico 'fw'.
            // A partir de agora a manipulação do arquivo deve ser feita
            // utilizando 'fw'.
            fw = new FileWriter (f);
            saida = new PrintWriter (fw); // Associar a saída de dados com 'fw'.
            
            saida.println(_texto); // Escrevendo no arquivo texto.
            
        }catch (IOException ioe){
        
            JOptionPane.showMessageDialog(null, "Erro ao escrever arquivo.");

        }finally{
            
            try {
                if (saida != null) saida.close(); // Fecha a ligação com 'fw'.
                if (fw != null) fw.close();       // Fecha o arquivo
            } catch (IOException ex) {
            
            }
            
        }
        
    }
    
}
