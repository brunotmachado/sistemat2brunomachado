package pacoteSBT2;

import javax.swing.JOptionPane;

/**
 *
 * @author Bruno Machado
 */
public class Principal {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String texto = "A aplicação foi incializada! ";
        
        Arquivo.lerArquivo();
        Arquivo.gerarLog(texto);
        
    }
    
    // Método para acessar o menu principal da aplicação
    private static int menuPrincipal(){
        
        return Integer.parseInt(JOptionPane.showInputDialog(null, 
                                                                  "=======================       \n"
                                                                + " 1 - Entrar na aplicação      \n"
                                                                + " 0 - Sair da aplicação        \n"
                                                                + "=======================       \n"
                                                                + "    Digite uma opção válida   \n"
                                                                + "=======================       \n",
                                                                     
                                                                "Trabalho 1", JOptionPane.QUESTION_MESSAGE));
        
    }
    
}
