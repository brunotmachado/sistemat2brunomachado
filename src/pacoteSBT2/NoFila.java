/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoteSBT2;

/**
 *
 * @author Bruno Machado
 */
    public class NoFila {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private
        String nomePessoa, email;

        int telefone;
    
        
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
        
    public NoFila(String _np, int _tp, String _ep){
        this.setNomePessoa(_np);
        this.setTelefone(_tp);
        this.setEmail(_ep);
    }
    
       
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/
        
    public String getNomePessoa() {
        return this.nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefone() {
        return this.telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }
        
}
