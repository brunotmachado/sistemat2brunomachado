package pacoteSBT2;

import java.util.HashSet;

/**
 *
 * @author Bruno Machado
 */
public class Pilha {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    NoPilha Pilha[];
    
    int topo;
    int quantidadeIngressos = 75;
    
    
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/

    public NoPilha[] getPilha() {
        return Pilha;
    }

    public void setPilha(NoPilha[] Pilha) {
        this.Pilha = Pilha;
    }

    public int getTopo() {
        return topo;
    }

    public void setTopo(int topo) {
        this.topo = topo;
    }

    public int getQuantidadeIngressos() {
        return quantidadeIngressos;
    }

    public void setQuantidadeIngressos(int quantidadeIngressos) {
        this.quantidadeIngressos = quantidadeIngressos;
    }
    
    
    
    // Método para criar pilha
    private void criarPilha(int _qp){
        this.setTopo(-1);
        this.setPilha(new NoPilha[_qp]);
        
        
    }
    
    // Esvaziar a pilha
    public void clear(Pilha p){
        if (!p.isEmpty(p)) {
            p.setTopo(-1);
        }
    }
    
    // Verificar se a pilha está vazia
    public boolean isEmpty(Pilha p){
        return p.getTopo() == -1;
    }
    
    // Verificar se a pilha está cheia
    public boolean isFull(Pilha p){
        return p.getTopo() == (p.getPilha().length-1);
    }
    
    // Empilhar dados na pilha
    public boolean push(Pilha p, int _cs){
        
        // Se a plha não estiver cheia 
        if (!p.isFull(p)) {
            p.setTopo(p.getTopo() +1); // Atualiza o topo
            // Cria um novo nó na posição topo,
            // insere os dados e insere na posição topo da pilha.
            p.getPilha()[topo] = new NoPilha(_cs);
            return true;
            
        }
        return false;
    }
    
    
    // Desempilhar dados da pilha
    public boolean pop(Pilha p){
        
        // Verifica se a pilha não está vazia
        // Caso não esteja vazia, apresenta os dados
        // que não estão na pilha
        
        if (p.top(p)) {
            p.setTopo(p.getTopo() -1);
            return true;
        }
        return false;
    }
    
    
    // Apresentar dados de quem está no topo
    public boolean top(Pilha p){
        
        // Se a pilha não estiver vazia
        // apresenta os dados do topo da pilha
        
        if (!p.isEmpty(p)) {
            // Apresenta os dados a serem desempilhados
            return true;
        }
        return false;
    }
    
    
    // Mostrando a quantidade de ingressos que estão na pilha
    public int size(Pilha p){
        return (p.getTopo()+1);
    }
    
    
}
