package pacoteSBT2;

/**
 *
 * @author Bruno Machado
 */
public class Agraciados {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    // Atributos Informações dos Agraciados
    
    private String nome;
    private int sala;
    private String telefone;
    private String email;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // Construtor vazio
    public Agraciados(){ 
    
    }
    
    // Construtor completo
    public Agraciados (String _nome, int _sala, int _telefone, String _email){
        
    }
    
    // Construtor sem telefone (pois é opcional)
    public Agraciados (String _nome, int _sala, String _email){
        
    }
    

    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/

    public String getNome() {
        return this.nome;
    }

    public void setNome(String _nome) {
        this.nome = _nome;
    }

    public int getSala() {
        return this.sala;
    }

    public void setSala(int _sala) {
        this.sala = _sala;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String _telefone) {
        this.telefone = _telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String _email) {
        this.email = _email;
    }
    
}
