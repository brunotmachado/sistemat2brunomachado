package pacoteSBT2;

/**
 *
 * @author Bruno Machado
 */
public class Lista {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private Agraciados[] Lista;
    private int FimLista;
    private int TamanhoVetor = 15;
    
    
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/
    
    public Agraciados[] getLista() {
        return Lista;
    }

    public void setLista(Agraciados[] Lista) {
        this.Lista = Lista;
    }

    public int getFimLista() {
        return FimLista;
    }

    public void setFimLista(int FimLista) {
        this.FimLista = FimLista;
    }

    public int getTamanhoVetor() {
        return TamanhoVetor;
    }

    public void setTamanhoVetor(int TamanhoVetor) {
        this.TamanhoVetor = TamanhoVetor;
    }
    
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // Contrutor da Lista
    // tv = tamanho do vetor
    public void ListaContigua(int tv){
        this.criarLista(tv);
    }
    
    // Criar Lista
    private void criarLista(int tv){
        this.Lista = new Agraciados[tv];
        this.setTamanhoVetor(tv);
        this.setFimLista(-1);
    }
    
    // Limpar Lista
    public void LimparLista(){
        this.setFimLista(-1);
    }
    
    // Verificar se a lista está vazia 
    public boolean isEmpty(){
        return this.FimLista == -1;
    }
    
    // Verificar se a lista está cheia
    public boolean isFull(){
        return this.FimLista == this.TamanhoVetor-1;
    }
           
    // Método para 
    
}
