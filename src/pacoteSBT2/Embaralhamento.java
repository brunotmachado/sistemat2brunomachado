package pacoteSBT2;

import java.util.Collections;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

/**
 *
 * @author Jefferson
 */
public class Embaralhamento {

    private static Integer[] numeros;
    private static List<Integer> vetor;

    public static Integer[] getNumeros() {
        return numeros;
    }

    public static void setNumeros(Integer[] numeros) {
        Embaralhamento.numeros = numeros;
    }

    public static List<Integer> getVetor() {
        return vetor;
    }

    public static void setVetor(List<Integer> vetor) {
        Embaralhamento.vetor = vetor;
    }
    
    public static void main(String[] args) {

        // Cria um array de números em ordem sequencial
        numeros = new Integer[]{1,2,3,4,5};
        
        // Array.asList() -> Retorna uma lista de tamanho fixo respaldada pela matriz especificada.
        // https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#asList-T...-
        
        // Coloca a lista de números dentro do atributo 'vetor'.
        List<Integer> vetor = Arrays.asList(numeros);
        
        // Apresentando os dados armazenados no atributo 'vetor'.
        JOptionPane.showMessageDialog(null, vetor, "Vetor original", JOptionPane.PLAIN_MESSAGE);
        
        // Embaralhando 15 vezes os dados do atributo 'vetor'.
        for (int cont = 1; cont <= 15; cont++){
        
            // shuffle() -> permutar aleatoriamente a lista especificada usando uma fonte padrão de aleatoriedade.
            // https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html#shuffle-java.util.List-
            Collections.shuffle(vetor); // Embaralha o vetor
            
            // Apresentando os dados embaralhados do atributo 'vetor'.
            JOptionPane.showMessageDialog(null, cont + "º embaralhamento : " + vetor, "Vetor embaralhado", JOptionPane.PLAIN_MESSAGE);
            
        }
        
    } 
  
}