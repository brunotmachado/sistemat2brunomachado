package pacoteSBT2;

import javax.swing.JOptionPane;

/**
 *
 * @author Bruno Machado
 */
public class Fila {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    NoFila Fila[];
    
    int inicioFila;
    int finalFila;
    
    int quantidadePessoas;
    
    
    /*-----------------------------------
           MÉTODOS get E set DA CLASSE
      -----------------------------------*/
    
    public NoFila[] getFila() {
        return Fila;
    }

    public void setFila(NoFila[] Fila) {
        this.Fila = Fila;
    }

    public int getInicioFila() {
        return inicioFila;
    }

    public void setInicioFila(int inicioFila) {
        this.inicioFila = inicioFila;
    }

    public int getFinalFila() {
        return finalFila;
    }

    public void setFinalFila(int finalFila) {
        this.finalFila = finalFila;
    }

    public int getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(int quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }
    
    
    // Criar Fila
    private void CriarFila(int _qp){
        this.setFila(new NoFila[_qp]);
        
        this.setInicioFila(0);
        this.setFinalFila(-1);
        this.setQuantidadePessoas(0);
    }
    
    // Limpar Fila
    public void LimparFila(Fila f){
        
        if(!f.isEmpty(f)){
            
            this.setInicioFila(0);
            this.setFinalFila(-1);
            this.setQuantidadePessoas(0);
        }
    }
    
    // Verificar se a Fila está vazia
    public boolean isEmpty(Fila f){
        return ( f.getFinalFila() < f.getInicioFila());
    }
    
    // Verifica se a fila está cheia
    public boolean isFull(Fila f){
        return ( f.getFinalFila() == (f.getFila().length-1));
    }
    
    // Enfileirar elemento na fila
    public boolean Enfileirar(Fila f, String _np, int _tp, String _ep){
        
        if(!f.isEmpty(f)){
            
            f.setFinalFila(f.getFinalFila()-1);
            f.getFila()[f.getFinalFila()] = new NoFila(_np, _tp, _ep);
            f.setQuantidadePessoas(f.getQuantidadePessoas()+1);
            
            return true;
        }
        return false;
    }
    
    // Desfileirar elemento da Fila
    public boolean Desfileirar(Fila f){
        
        if (f.front(f)) {
            
            for (int i = f.getInicioFila(); i < f.getFinalFila(); i++) {
                f.getFila()[i] = f.getFila()[i+1];
            
            }
            f.setFinalFila(f.getFinalFila()-1);
            f.setQuantidadePessoas(f.getQuantidadePessoas()-1);
            
            return true;
                        
        }
        return false;
    }
    
    // Acessar primeiro elemento da fila
    public boolean front(Fila f){
        
        if(!f.isEmpty(f)) {
            
           // Apressentar os dados
           return true;
           
        }
        return false;
    }
    
    // Percorrer a fila
    public boolean print(Fila f){
        
        if (!f.isEmpty(f)) {
            
            for (int i = f.getInicioFila(); i < f.getFinalFila(); i++) {
                //Mostrar Dados
            }
            return true;
        }
        
        return false;
    }
    
    // Acessar tamanho da fila
    public int size (Fila f){
        return f.getQuantidadePessoas();
    }
    
}
